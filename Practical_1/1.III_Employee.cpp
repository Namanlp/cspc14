//
// Created by naman on 18/04/22.
//

#include <iostream>
#include <string>

struct Employee{
private:
    std::string Employee_name;
    std::string Employee_id;
    std::string Employee_salary;
    std::string Employee_address;
public:
    static void takeInput(Employee employee1){
        std::cout << "Enter Employee name : ";
        std::cin >> employee1.Employee_name;
        std::cout << "Enter Employee id : ";
        std::cin >> employee1.Employee_id;
        std::cout << "Enter Employee salary : ";
        std::cin >> employee1.Employee_salary;
        std::cout << "Enter Employee address : ";
        std::cin >> employee1.Employee_address;

        Employee::print(employee1);
    }

    // Const pointer is used to prevent copying of data again and again.
    static void print(const Employee &employee1){
        std::cout << "========================================================================" << std::endl;
        std::cout << "Employee's name is : " << employee1.Employee_name << std::endl;
        std::cout << "Employee's id is : " << employee1.Employee_id << std::endl;
        std::cout << "Employee's salary is : " << employee1.Employee_salary << std::endl;
        std::cout << "Employee's address is : " << employee1.Employee_address << std::endl;
        std::cout << "========================================================================" << std::endl;
    }
};

int main(){
    int num = 0;
    std::cout << "Enter number of employees : " ;
    std::cin >> num;
    while (num--){
        Employee employee1;
        Employee::takeInput(employee1);
    }
}