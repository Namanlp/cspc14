//
// Created by naman on 2/5/22.
//

//  Create the database of students using Class, having the following attributes :
//roll_no, student_name, student_address, student_city, student_pin, student_sem, rank, and
//branch. Also write the program to enter the data for 500 students in any order and then display
//the list of students for a given branch and semester on display.

#include <iostream>
using namespace std;

int compareStr(const char string1[3], const char string2[3]){
    for (int i = 0; i <3; i++)
        if (string1[i] != string2[i])
            return 0;
    return 1;
}



class Student{
private:
    int roll_no = 0;
    char student_name[100]{};
    char student_address[100]{};
    char student_city[100]{};
    long student_pin = 0;
    short student_sem = 1;
    long rank = 1;
    char branch[3]{};
public:
    static void printStudents( Student studentList[], int totalStudents = 0){
        char studentBranch[3];
        cout << "Enter student branch : ";
        cin >> studentBranch;
        int sem = 1;
        cout << "Enter student semester : ";
        cin >> sem;
        for (int i = 0; i < totalStudents; i++){
            if (studentList[i].student_sem == sem && compareStr(studentBranch, studentList[i].branch)){
                cout << "==================================================================" << endl;
                cout << "Students Name : " << studentList[i].student_name << endl;
                cout << "Students roll number : " << studentList[i].roll_no << endl;
                cout << "Students address  : " << studentList[i].student_address << endl;
                cout << "Students City : " << studentList[i].student_city << endl;
                cout << "Students Pin : " << studentList[i].student_pin << endl;
                cout << "Students Semester : " << studentList[i].student_sem << endl;
                cout << "Students Rank : " << studentList[i].rank << endl;
                cout << "Students Branch : " << studentList[i].branch << endl;
            }
        }
    }

    void static getStudents(Student studentList[], int totalStudents = 0){
        for (int i = 0; i < totalStudents; i++){
            cout << "==================================================================" << endl;
            cout << "Enter student number " << i+1 << " roll_no, student_name, student_address, student_city, student_pin, student_sem, rank and branch : ";
            cin >> studentList[i].roll_no >> studentList[i].student_name >> studentList[i].student_address >> studentList[i].student_city >> studentList[i].student_pin >> studentList[i].student_sem >> studentList[i].rank >> studentList[i].branch;
        }
    }
};

int main(){
    Student studentList[500];
    int numberList = 0;
    cout << "Enter number of students ( upto 500 ) : " ;
    cin >> numberList;
    Student::getStudents(studentList, numberList);
    Student::printStudents(studentList, numberList);
}