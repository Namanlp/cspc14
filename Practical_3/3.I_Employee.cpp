//
// Created by naman on 29/4/22.
//

// 3.I
//Create a class called Employee that contains a name (an array of char) and an employee
//number (type long). Include a member function called getData() to get data from the user for
//insertion into the object, and another function called putData() to display the data. Assume the
//name has no embedded banks.
//Write a main() program to exercise this class. It should create an array of type employee, and
//then invite the user to input data for up to 100 employees. Finally, it should print out the data
//for all the employees.

#include <iostream>
using namespace std;

class Employee{
private:
    char name[100]{};
    long number {};

public:
    void getData(){
        cout << "Enter Employee Name : " << endl;
        cin >> name;
        cout << "Enter Employee Number : " << endl;
        cin >> number;

    }
    void putData(){
        cout << "Employee Name : " << name << endl;
        cout << "Employee Number : " << number << endl;
    }
};

int main(){
    Employee empArray[100];
    int numberList = 0;
    cout << "Enter number of employee ( upto 100 ) : " ;
    cin >> numberList;
    for (int i =0 ; i<numberList;i++){
        cout << "Enter data of Employee number " << i+1 << endl;
        empArray[i].getData();
    }
    cout << "=========================================================" << endl;
    for (int i =0 ; i<numberList;i++){
        cout << "Data of Employee number " << i+1 << " is " << endl;
        empArray[i].putData();
    }
}

