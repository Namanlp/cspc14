//
// Created by naman on 25/4/22.
//
// 2.III  Define a class to represent a bank account. Include the following members:
//Data Members: Name of the depositor, Account no. , Type of account, Balance amount.
// Member Functions: To assign initial values, To deposit an amount,To withdraw an amount after checking the balance,
//To display name and balance.
//Write a main program to test the program.

#include <iostream>
#include <string>

class BankAccount{
private:
//    char nameOfDepositor[100];
    std::string nameOfDepositor;
    long long int accountNumber;
    std::string typeOfAccount;
    long long int balanceAmount;

public:
    BankAccount(std::string name, long long int number, std::string type, long long int balance ){
        nameOfDepositor = name;
        accountNumber = number;
        typeOfAccount = type;
        balanceAmount = balance;
    }
    //To deposit an amount
    void depositAmount(long long int amount){
        std::cout << "\nBalance before deposit : " << balanceAmount;
        balanceAmount += amount;
        std::cout << "\nBalance after deposit : " << balanceAmount << std::endl;
    }
    void withdrawAmount(long long int amount){
        std::cout << "\nBalance before withdraw : " << balanceAmount;
        balanceAmount -= amount;
        std::cout << "\nBalance after withdraw : " << balanceAmount << std::endl;
    }
    void displayInfo(){
        std::cout << "\nName : " << nameOfDepositor << "\nAccount Number : " << accountNumber << "\nBalance : " << balanceAmount << std::endl;
    }
};

int main(){
    std::cout << "Number of bank accounts : " ;
    int n = 0;
    std::cin >> n;
    while (n--){
        std::string nameOfDepositor;
        long long int accountNumber;
        std::string typeOfAccount;
        long long int balanceAmount;
        std::cout << "Enter Depositor name : ";
        std::cin >> nameOfDepositor;
        std::cout << "Enter account number : ";
        std::cin >> accountNumber;
        std::cout << "Enter type of account : ";
        std::cin >> typeOfAccount;
        std::cout << "Enter balance amount : ";
        std::cin >> balanceAmount;
        BankAccount account1(nameOfDepositor,accountNumber, typeOfAccount, balanceAmount);
        std::cout << "\n================================================================================================" <<std::endl;
        account1.displayInfo();
        std::cout << "\n================================================================================================" <<std::endl;
        std::cout << "Deposit / Withdraw (1/2) : ";
        int choice = 0;
        std::cin >> choice;
        std::cout << "\n Enter Amount : ";
        long long int amount = 0;
        std::cin >> amount;
        if (choice == 1)
            account1.depositAmount(amount);
        else if (choice == 2)
            account1.withdrawAmount(amount);
        else
            std::cout << "\n Sorry, wrong choice" << std::endl;
        }
}