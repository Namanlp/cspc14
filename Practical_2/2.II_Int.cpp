//
// Created by naman on 25/4/22.
//
// Create a class that initiates part of functionality of the basic data type int. Call the class
//Int(note different spelling). The only data in this class is an int variable. Include member
//functions to initialize an Int to 0, to initialize it to an int value, to display it( looks just like an
//int), and to add two Int values. Write a program that exercises this class by creating two
//initialized and one uninitialized Int values, adding these two initialized values and placing the
//response in the uninitialized value, and then displaying this result.

#include <iostream>

class Int{
private:
    int variable;
public:
    Int(){
        variable = 0;
    }
    Int(int a){
        variable = a;
    }
    void print(){
        std::cout << variable;
    }
    int add(Int a, Int b){
        return a.variable + b.variable;
    }
};

int main(){
    int temp = 0;
    std::cout << "Enter First Integer : ";
    std::cin >> temp;
    Int int1(temp), int2(10), int3;
    std::cout << "\n\nSecond Integer : ";
    int2.print();
    std::cout << "\nThird Integer : ";
    int3.print();
    std::cout << std::endl;
    int result = int1.add(int1, int2);
    std::cout << "Sum of first and second integer  : " << result << std::endl;
}