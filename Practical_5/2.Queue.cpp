//
// Created by naman on 5/6/22.
//

#include <iostream>
using namespace std;

class Queue{
private:
    int filled {};
    int array[100] {};

public:
    void insertQueue(int number){
        if (filled == 100){
            cout << "Sorry, Queue is already filled. Please remove a few elements first" << endl;
            return;
        }
        for (int i = filled; i > 0; i--)
            array[i]= array[i-1];
        array[0] = number;
        filled++;
    }

    void deleteQueue(){
        if (filled == 0){
            cout << "Sorry, Queue is already empty. Please add a few elements first" << endl;
            return;
        }

        cout << array[filled - 1] << " is removed from queue" << endl;
        array[filled-1] = 0;
        filled--;
    }

    void display() const{
        cout << "Queue is : [ ";
        for (int i = 0; i < filled - 1 ; i++)
            cout  << array[i] << ", ";
        cout << array[filled-1] << " ]" << endl;
    }
};

int main(){
    Queue givenQueue;
    int choice;
    do{
        cout << "==============================================================================" << endl;
        cout << "1 : Insert an element into queue." << endl;
        cout << "2 : Delete an element from queue." << endl;
        cout << "3 : Print Queue" << endl;
        cout << "4 : Quit" << endl;

        cout << "Enter Choice : ";
        cin >> choice;

        cout << "==============================================================================" << endl;

        switch (choice) {
            case 1: cout << "Enter Element : " ; int number; cin >> number; givenQueue.insertQueue(number);  break;
            case 2: givenQueue.deleteQueue(); break;
            case 3: givenQueue.display(); break;
            case 4: cout << "Thanks" << endl; break;
            default: cout << "Wrong Choice ! Please enter a valid choice;" << endl;
        }
    } while (choice != 4);

    return 0;
}