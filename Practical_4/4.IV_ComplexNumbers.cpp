//
// Created by naman on 29/5/22.
//

#include <iostream>
using namespace std;

class complexNumber{
private:
    double real {}, imaginary {};

public:
    // ==================================== Constructors ================================================

    complexNumber(double r, double i){
        real = r;
        imaginary = i;
    }

    complexNumber(){
        real = 0;
        imaginary = 0;
    }

    // ==================================== Operator Overloading ===========================================

    complexNumber operator + (complexNumber num2) const{
        complexNumber num3 {};
        num3.real = real + num2.real;
        num3.imaginary = imaginary + num2.imaginary;
        return num3;
    }
    complexNumber operator - (complexNumber num2) const{
        complexNumber num3 {};
        num3.real = real - num2.real;
        num3.imaginary = imaginary - num2.imaginary;
        return num3;
    }
    complexNumber operator * (complexNumber num2) const{
        complexNumber num3 {};
        num3.real = ( real * num2.real ) - ( imaginary * num2.imaginary );
        num3.imaginary = ( real *num2.imaginary ) + ( imaginary *num2.real );
        return num3;
    }

    // ==================================== Member functions ============================================

    void displayComplex() const{
        cout << real << " ";
        (imaginary<0)? cout << "- "<< -1*imaginary : cout << "+ " << imaginary;
        cout << "i ";
    }


};

int main(){
    double r, i;
    cout << "Enter first complex number : " ;
    cin >> r >> i;
    complexNumber num1(r,i);
    cout << "Enter second complex number : " ;
    cin >> r >> i;
    complexNumber num2(r,i);
    cout << "\nAddition of the two numbers gives : ";
    (num1 + num2).displayComplex();
    cout << "\nSubtracting second number from the first number gives : ";
    (num1 - num2).displayComplex();
    cout << "\nMultiplication of the two numbers gives : ";
    (num1 * num2).displayComplex();

}