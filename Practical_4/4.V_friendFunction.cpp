//
// Created by naman on 30/5/22.
//

#include <iostream>
using namespace std;

class number{
private:
    double num;
public:

    explicit number(int givenNum){
        num = givenNum;
    }

    friend int compare(number num1, number num2);
};

int compare(number num1, number num2){
    int returnNum = 0;
    if (num1.num == num2.num)
        return returnNum;
    ( num1.num > num2.num )? returnNum = 1 : returnNum = -1;
    return returnNum;
}

int main(){
    int num = 0;
    cout << "Enter first number : " ;
    cin >> num;
    number num1(num);
    cout << "Enter second number : " ;
    cin >> num;
    number num2(num);
    int result = compare(num1, num2);

    switch (result) {
        case 1: cout << "First number is greater than second number."; break;
        case 0: cout << "Both numbers are equal."; break;
        case -1: cout << "Second number is greater than first number."; break;
        default: cout << "Somethings wrong. Please try latter";
    }
}