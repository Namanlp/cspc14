//
// Created by naman on 30/5/22.
//
#include <iostream>
using namespace std;

class Fraction{

private:
    int denominator = 0;
    int numerator = 0;

public:
    Fraction(int n, int d){
        numerator = n;
        denominator = d;
    }
    friend Fraction operator /= (Fraction fr1,Fraction fr2);

    void displayFraction() const{
        cout << numerator << " / " << denominator;
    }
};

Fraction operator /= (Fraction fr1,Fraction fr2){
    Fraction fr3(fr1.numerator*fr2.denominator,fr1.denominator*fr2.numerator);
    return fr3;
}

int main(){
    int d = 0, n = 0;

    cout << "Enter numerator and denominator for first fraction : " ;
    cin >> n >> d;
    Fraction fr1( n , d);
    cout << "Enter numerator and denominator for second fraction : " ;
    cin >> n >> d;
    Fraction fr2( n, d);
    cout << "Fraction 1 divided by second gives : ";
    (fr1 /= fr2).displayFraction();
}