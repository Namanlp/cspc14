//
// Created by naman on 26/6/22.
//

#include <iostream>
using namespace std;

class student{
protected:
    int rollNumber;
    char Name[100];
};


class exam{
protected:
    int marks[6];
};

class result : private student, private exam{
private:
    int totalMarks;
public:
    int calcMarks(){
        totalMarks = 0;
        for (int number : marks){
            totalMarks += number;
        }
        return totalMarks;
    }
    void setResult(int roll,char const name[], int const marksArray[]){
        rollNumber = roll;
        int i = 0;
        while ( name[i] != '\0' && i <= 100){
            Name[i] = name[i];
            i++;
        }
        for(i = 0; i < 6; i++)
            marks[i] = marksArray[i];
    }

    void showResult(){
        calcMarks();
        cout << "\nMarks are : ";
        for (int mark : marks)
            cout << mark << ", ";
        cout << "\nTotal Marks : " << totalMarks;
    }
};

int main(){

    char name[100];
    int number, roll, marks[6];
    cout << "Enter number of students : ";
    cin >> number;
    result resultsArray[number];
    for (int i = 0; i < number; i++){
        cout << "Enter name and roll number : ";
        cin >> name >> roll;
        cout << "Enter marks in all 6 subjects : ";
        for (int & mark : marks)
            cin >> mark;
        resultsArray[i].setResult(roll, name, marks);
    }

    for (result result : resultsArray){
        result.showResult();
    }


}