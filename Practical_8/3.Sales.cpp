//
// Created by naman on 26/6/22.
//

#include <iostream>
using namespace std;

class sales{
protected:
    float salesMonth[3];
};

class publication : public sales{
protected:
    char title[100] {};
    float price {};

public:
    virtual void getData(char const titleName[], float const cost){
        price = cost;
        int i = 0;
        while ( titleName[i] != '\0' && i <= 100){
            title[i] = titleName[i];
            i++;
        }
    }

    virtual void getData(){
        cout << "\nEnter title and Price for publication : ";
        cin >> title >> price;

        cout << "Enter sales of last 3 months : ";
        cin >> salesMonth[0] >> salesMonth[1] >> salesMonth[2];
    }

    virtual void putData(){
        cout << "\nTitle and Price for publication : ";
        cout << title << " " << price << endl;

        cout  << "Sales of last three months : " << salesMonth[0] << " "<< salesMonth[1] << " "<< salesMonth[2] <<endl;
    }
};

class book: private publication, public sales{
private:
    int pageCount {};
public:
    void getData(){
        cout << "\nEnter title, price and page count for book  : ";
        cin >> title >> price >> pageCount;

        cout << "Enter sales of last 3 months : ";
        cin >>  publication::salesMonth[0] >> publication::salesMonth[1] >> publication::salesMonth[2];
    }
    void putData(){
        cout << "\nTitle, price and page count for book  : ";
        cout << title << " " << price << " " << pageCount << endl;

        cout  << "Sales of last three months : " << publication::salesMonth[0] << " "<< publication::salesMonth[1] << " "<< publication::salesMonth[2] <<endl;
    }
};

class tape: private publication,private sales{
private:
    float playTime{};
public:
    void getData(){
        cout << "\nEnter title, price and play time for tape  : ";
        cin >> title >> price >> playTime;

        cout << "Enter sales of last 3 months : ";
        cin >>  publication::salesMonth[0] >> publication::salesMonth[1] >> publication::salesMonth[2];
    }
    void putData(){
        cout << "\nTitle, price and play time for tape  : ";
        cout << title << " " << price << " " << playTime << endl;

        cout  << "Sales of last three months : " << publication::salesMonth[0] << " "<< publication::salesMonth[1] << " "<< publication::salesMonth[2] <<endl;
    }
};

int main(){
    publication Publication1;
    book book1;
    tape tape1;

    Publication1.getData();
    book1.getData();
    tape1.getData();
    Publication1.putData();
    book1.putData();
    tape1.putData();
}