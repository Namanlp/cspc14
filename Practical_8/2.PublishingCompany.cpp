//
// Created by naman on 26/6/22.
//

#include <iostream>
using namespace std;

class publication{
protected:
    char title[100] {};
    float price {};

public:
    virtual void getData(char const titleName[], float const cost){
        price = cost;
        int i = 0;
        while ( titleName[i] != '\0' && i <= 100){
            title[i] = titleName[i];
            i++;
        }
    }

    virtual void getData(){
        cout << "\nEnter title and Price for publication : ";
        cin >> title >> price;
    }

    virtual void putData(){
        cout << "\nTitle and Price for publication : ";
        cout << title << " " << price;
    }
};

class book: private publication{
private:
    int pageCount;
public:
    void getData(){
        cout << "Enter title, price and page count for book  : ";
        cin >> title >> price >> pageCount;
    }
    void putData(){
        cout << "\nTitle, price and page count for book  : ";
        cout << title << " " << price << " " << pageCount;
    }
};

class tape: private publication{
private:
    float playTime;
public:
    void getData(){
        cout << "Enter title, price and play time for tape  : ";
        cin >> title >> price >> playTime;
    }
    void putData(){
        cout << "\nTitle, price and play time for tape  : ";
        cout << title << " " << price << " " << playTime;
    }
};

int main(){
    publication Publication1;
    book book1;
    tape tape1;

    Publication1.getData();
    book1.getData();
    tape1.getData();
    Publication1.putData();
    book1.putData();
    tape1.putData();
}

